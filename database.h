#ifndef DATABASE_H
#define DATABASE_H

// Don't #include any other file
#include "record.h"
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#include "cmpt_error.h"
#include <sstream>
#include <algorithm>
#include <stdlib.h>

class Database {
//Vector that stores artist records
    vector<Record> db;
    

public:
    Database();
//Reads in file with records into fb
    void readIn (string x);
//Adds a record into database
    void add(Record &r); 
//Prints the database
    void printDb();
//Search functions for each field in record
    vector <Record> nameSearch(string s);
    vector <Record> ageSearch(int i);
    vector <Record> genreSearch(string s);
    vector <Record> albumSearch(int i);
//Add record through menu
    void menuAdd();
//Returns the vector for public access
    vector <Record> const returnVec();
//Additional search functions (Search by substring, Range)
    vector <Record> name_sSearch(string s);
    vector <Record> age_sSearch(int i, int p);
    vector <Record> genre_sSearch(string s);
    vector <Record> album_sSearch(int i, int p); 
//Delete a record
    void delRec_name(string &s);
    void delRec_sName(string &s);
    void delRec_age(int i);
    void delRec_ageRange(int i, int p);
    void delRec_genre(string &s);
    void delRec_sGenre(string &s);
    void delRec_albums(int i);
    void delRec_albumsRange(int i, int p);
//List records
    vector <Record> nameAlph();
    vector <Record> name_rAlph();
    vector <Record> age_asc();
    vector <Record> age_dsc();
    vector <Record> genreAlph();
    vector <Record> genre_rAlph();
    vector <Record> albums_asc();
    vector <Record> albums_dsc();

};
 

#endif