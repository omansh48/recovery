#include "record.h"
#include "database.h"
#include "menu.h"
#include <string>

using namespace std;

int main() {

    string file = "db.txt";
    
    Database d;
    
    Database* d_point = &d;

    d.readIn(file);
    
    //string s = "Brun";

    //d.delRec_age(22);

    Menu menu(d_point);

    menu.mainMenu();

    d.printDb();


}