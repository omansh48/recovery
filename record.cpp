#include "record.h"
#include "database.h"
#include "menu.h"


using namespace std;

Record::Record()
: name(""), age(1), genre(""), albums(0) {};
Record::Record(string s,int a,string g, int album)
: name(s), age(a), genre(g), albums(album) {}

Record::Record(const Record &other) : name(other.name),
age(other.age), genre(other.genre), albums(other.albums){}

string Record::get_name() const {return name;}
int Record::get_age() const {return age;}
string Record::get_genre() const {return genre;}
int Record::get_albums() const {return albums;}

void Record::set_name(string &s){name = s;}
void Record::set_age(int x){age = x;}
void Record::set_genre(string &s){genre = s;}
void Record::set_albums(int x){albums=x;}
